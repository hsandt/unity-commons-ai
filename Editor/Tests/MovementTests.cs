﻿using UnityEngine;
using System.Collections;

using NUnit.Framework;

[TestFixture]
public class MovementTests {

	[Test]
	public void GetNewOrientation_NullVelocity_CurrentOrientation () {
		Quaternion currentOrientation = Quaternion.FromToRotation(Vector2.up, -Vector2.right);
		Quaternion newOrientation = MovementUtils.GetNewOrientation(currentOrientation, Vector2.zero);
		Assert.AreEqual(currentOrientation, newOrientation);
	}

	[Test]
	public void GetNewOrientation_RightVelocity_RightOrientation () {
		Quaternion currentOrientation = Quaternion.FromToRotation(Vector2.up, -Vector2.right);
		Quaternion newOrientation = MovementUtils.GetNewOrientation(currentOrientation, Vector2.right);
		Assert.AreEqual(Quaternion.FromToRotation(Vector2.up, Vector2.right), newOrientation);
	}

	// Update is called once per frame
	void Update () {

	}
}
