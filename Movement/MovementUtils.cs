﻿using UnityEngine;
using System.Collections;

public class MovementUtils {

	/// Return orientation matching velocity, or currentOrientation if velocity is null or almost null
	public static Quaternion GetNewOrientation(Quaternion currentOrientation, Vector2 velocity) {
		if (velocity.normalized != Vector2.zero) {
			// we use Vector2.up as the reference (angle = 0)
			return Quaternion.FromToRotation(Vector2.up, velocity);
		}
		else {
			return currentOrientation;
		}
	}

}
