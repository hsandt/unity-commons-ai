﻿using UnityEngine;
using System.Collections;

public struct SteeringOutput {
	public Vector2 linear;  // steering linear acceleration
	public float angular;  // steering angular acceleration

	public SteeringOutput(Vector2 linear, float angular) {
		this.linear = linear;
		this.angular = angular;
	}
}
