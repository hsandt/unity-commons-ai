﻿using UnityEngine;
using System.Collections;

public abstract class KinematicMovement {

	// reference to moving character transform
	protected Transform m_CharacterTransform;

	public KinematicMovement(Transform characterTr) {
		m_CharacterTransform = characterTr;
	}

	public abstract KinematicSteeringOutput GetSteering();

}

public abstract class TargetKinematicMovement : KinematicMovement {

	// reference to target
	protected Transform m_Target;

	public TargetKinematicMovement(Transform characterTr, Transform target) : base (characterTr) {
		m_Target = target;
	}

}

public class KinematicSeek : TargetKinematicMovement {

	// params
	float m_MaxSpeed;

	public KinematicSeek(Transform characterTr, Transform target, float maxSpeed) : base(characterTr, target) {
		m_MaxSpeed = maxSpeed;
	}

	public override KinematicSteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new KinematicSteeringOutput();

		// get the direction of the target
		steering.velocity = m_Target.position - m_CharacterTransform.position;

		// the velocity is along this direction, at full speed
		steering.velocity.Normalize();
		steering.velocity *= m_MaxSpeed;

		// face the direction we want to move toward
		// REFACTOR: this is a Get() method, so prefer modifying orientation outside? But KinematicSteeringOutput only contains relative rotation...
		m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.velocity);

		return steering;
	}
}

public class KinematicArrive : TargetKinematicMovement {

	// params
	float m_MaxSpeed;
	float m_StopRadius;  // internal satisfaction radius
	float m_DecelerateRadius;  // external radius where the character starts to decelerate (m_MaxSpeed * timeToTarget)

	public KinematicArrive(Transform characterTr, Transform target, float maxSpeed, float stopRadius, float decelerateRadius) : base(characterTr, target) {
		m_MaxSpeed = maxSpeed;
		m_StopRadius = stopRadius;
		m_DecelerateRadius = decelerateRadius;
	}

	public override KinematicSteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new KinematicSteeringOutput();

		// get the direction of the target
		Vector2 vectorToTarget = m_Target.position - m_CharacterTransform.position;

		// check if we are within radius
		if (vectorToTarget.magnitude < m_StopRadius) {
			return steering;  // contains default velocity and angular velocity of zero
		}

		// compute final velocity, clamp if needed: before decelerate radius is reached, move normally; else slow down
		steering.velocity = Vector2.ClampMagnitude(vectorToTarget / m_DecelerateRadius * m_MaxSpeed, m_MaxSpeed);

		// face the direction we want to move toward by setting the orientation manually (and angular velocity is 0)
		// REFACTOR: this is a Get() method, so prefer modifying orientation outside? But KinematicSteeringOutput only contains relative rotation...
		m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.velocity);

		return steering;
	}
}

public class KinematicFlee : TargetKinematicMovement {

	// params
	float maxSpeed;

	public KinematicFlee(Transform characterTr, Transform target, float _maxSpeed) : base(characterTr, target) {
		maxSpeed = _maxSpeed;
	}

	public override KinematicSteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new KinematicSteeringOutput();

		// get the direction of the target
		steering.velocity = m_CharacterTransform.position - m_Target.position;

		// the velocity is along this direction, at full speed
		steering.velocity.Normalize();
		steering.velocity *= maxSpeed;

		// face the direction we want to move toward
		m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.velocity);

		return steering;
	}
}

public class KinematicWander : KinematicMovement {

	// params
	float maxSpeed;
	float maxRotation;
	float rotationTimeInterval = 1f;  // the character changes orientation suddenly every rotationTimeInterval

	// state vars
	float timeBeforeNextRotation;

	public KinematicWander(Transform characterTr, float _maxSpeed, float _maxRotation) : base(characterTr) {
		maxSpeed = _maxSpeed;
		maxRotation = _maxRotation;
		timeBeforeNextRotation = 0;  // later, when KinematicWander is created only once, put that in Init/Reset movement method instead, called in state Enter()
	}

	public override KinematicSteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new KinematicSteeringOutput();

		// the velocity is along the currently facing direction, at full speed
		steering.velocity = m_CharacterTransform.up;
		steering.velocity *= maxSpeed;

		// orientation varies with random binomial law around current orientation (forward for CCW angle in left-handed coord system), regularly
		timeBeforeNextRotation -= Time.deltaTime;  // deltaTime from FixedUpdate of FSMAIControl
		if (timeBeforeNextRotation < 0) {
			timeBeforeNextRotation = rotationTimeInterval;
			float randomAngle = (Random.value - Random.value) * maxRotation;  // between -maxRotation and +maxRotation, peak at 0, per second
			m_CharacterTransform.rotation = Quaternion.AngleAxis(randomAngle, Vector3.forward) * m_CharacterTransform.rotation;
		}

		return steering;
	}
}
