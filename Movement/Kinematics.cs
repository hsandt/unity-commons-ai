﻿using UnityEngine;
using System.Collections;

public struct KinematicSteeringOutput {
	public Vector2 velocity;
	public float rotation;

	public KinematicSteeringOutput(Vector2 velocity, float rotation) {
		this.velocity = velocity;
		this.rotation = rotation;
	}
}
