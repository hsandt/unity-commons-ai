﻿using UnityEngine;
using System.Collections;

public abstract class SteeringMovement {

	// reference to moving character transform
	protected Transform m_CharacterTransform;

	public SteeringMovement(Transform characterTr) {
		m_CharacterTransform = characterTr;
	}

	public abstract SteeringOutput GetSteering();

}

public abstract class TargetSteeringMovement : SteeringMovement {

	// reference to target
	protected Transform m_Target;

	public TargetSteeringMovement(Transform characterTr, Transform target) : base (characterTr) {
		m_Target = target;
	}

}

public class Seek : TargetSteeringMovement {

	// params
	float m_MaxAccel;

	public Seek(Transform characterTr, Transform target, float maxAccel) : base(characterTr, target) {
		m_MaxAccel = maxAccel;
	}

	public override SteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new SteeringOutput();

		// get the direction of the target
		steering.linear = m_Target.position - m_CharacterTransform.position;

		// the acceleration is along this direction, at full speed
		steering.linear.Normalize();
		steering.linear *= m_MaxAccel;

		// face the direction we want to move toward (prefer variable matching, and need to get current / new velocity from accel here)
		// m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.velocity);

		return steering;
	}
}

public class Arrive : TargetSteeringMovement {

	protected Rigidbody2D characterRigidbody2D;

	// params
	float m_MaxAccel;
	float m_MaxSpeed;
	float m_StopRadius;  // internal satisfaction radius
	float m_DecelerateRadius;  // external radius where the character starts to decelerate (m_MaxSpeed * timeToTarget)
	float m_TimeToTargetVelocity;  // time until target velocity is reached

	// "updatable" version: need manual Setup() after construction
	public Arrive(Transform characterTr, Transform target) : base(characterTr, target) {
		characterRigidbody2D = characterTr.GetComponentOrFail<Rigidbody2D>();
	}

	// "all params provided" version
	public Arrive(Transform characterTr, Transform target, float maxAccel, float maxSpeed, float stopRadius, float decelerateRadius, float timeToTargetVelocity) : base(characterTr, target) {
		characterRigidbody2D = characterTr.GetComponentOrFail<Rigidbody2D>();
		Setup(maxAccel, maxSpeed, stopRadius, decelerateRadius, timeToTargetVelocity);
	}

	public void Setup(float maxAccel, float maxSpeed, float stopRadius, float decelerateRadius, float timeToTargetVelocity) {
		m_MaxAccel = maxAccel;
		m_MaxSpeed = maxSpeed;
		m_StopRadius = stopRadius;
		m_DecelerateRadius = decelerateRadius;
		m_TimeToTargetVelocity = timeToTargetVelocity;
	}

	public override SteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new SteeringOutput();

		// get the direction of the target
		Vector2 vectorToTarget = m_Target.position - m_CharacterTransform.position;

		Vector2 targetVelocity;

		// if we are within radius, set target velocity to zero
		// note: unlike 'AI for Games', we prefer decelerating to zero velocity rather than doing nothing and waiting for drag to stop us
		if (vectorToTarget.magnitude < m_StopRadius) {
			targetVelocity = Vector2.zero;
		} else {
			// compute final velocity, clamp if needed: before decelerate radius is reached, move normally; else slow down
			targetVelocity = Vector2.ClampMagnitude(vectorToTarget / m_DecelerateRadius * m_MaxSpeed, m_MaxSpeed);
		}

		// set acceleration so that velocity is reached within timeToTargetVelocity (actually a bit longer, since accel will decrease when distance decreases)
		Vector2 acceleration = (targetVelocity - characterRigidbody2D.velocity) / m_TimeToTargetVelocity;
		steering.linear = Vector2.ClampMagnitude(acceleration, m_MaxAccel);

		// face the direction we want to move toward by setting the orientation manually (and angular velocity is 0)
		// REFACTOR: this is a Get() method, so prefer modifying orientation outside? But SteeringOutput only contains relative rotation...
		// m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.linear);

		return steering;
	}
}

/// Rarely used movement to leave with acceleration
public class Leave : TargetSteeringMovement {

	// params
	float m_MaxAccel;

	public Leave(Transform characterTr, Transform target, float _maxSpeed) : base(characterTr, target) {
		m_MaxAccel = _maxSpeed;
	}

	public override SteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new SteeringOutput();

		// get the direction of the target
		steering.linear = m_CharacterTransform.position - m_Target.position;

		// the acceleration is along this direction, at full speed
		steering.linear.Normalize();
		steering.linear *= m_MaxAccel;

		// face the direction we want to move toward
		// m_CharacterTransform.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, steering.linear);

		return steering;
	}
}
/// Align with target with acceleration
public class Align : TargetSteeringMovement {

	protected Rigidbody2D characterRigidbody2D;

	// params
	float m_MaxAngularAcceleration;
	float m_MaxRotationSpeed;  // max rotation speed allowed
	float m_StopRadius;  // internal satisfaction rotation angle range
	float m_DecelerateRadius;  // external radius where the character starts to decelerate in rotation speed (m_MaxRotation * timeToTarget)
	float m_TimeToTargetVelocity;  // time until target rotation is reached


	public Align(Transform characterTr, Transform target, float maxAngularAcceleration, float maxRotationSpeed, float stopRadius, float decelerateRadius, float timeToTargetVelocity) : base(characterTr, target) {
		characterRigidbody2D = characterTr.GetComponentOrFail<Rigidbody2D>();
		Setup(maxAngularAcceleration, maxRotationSpeed, stopRadius, decelerateRadius, timeToTargetVelocity);
	}

	public void Setup(float maxAccel, float maxRotationSpeed, float stopRadius, float decelerateRadius, float timeToTargetVelocity) {
		m_MaxAngularAcceleration = maxAccel;
		m_MaxRotationSpeed = maxRotationSpeed;
		m_StopRadius = stopRadius;
		m_DecelerateRadius = decelerateRadius;
		m_TimeToTargetVelocity = timeToTargetVelocity;
	}

	public override SteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new SteeringOutput();

		// get the naive orientation to the target

		// (a) with vectors and quaternions
		Vector2 targetForward = m_Target.rotation * Vector2.up;
		Vector2 characterForward = m_CharacterTransform.rotation * Vector2.up;
		Quaternion rotation = Quaternion.FromToRotation(characterForward, targetForward);

		// (b) with quaternions
		// all rotations are around the same axis, so u * v^-1 is the relative rotation to go from v to u
		// Quaternion rotation = m_Target.rotation * Quaternion.Inverse(m_CharacterTransform.rotation);

		// (a) or (b) convert quaternion to axis-angle, keep angle which is absolute (we know axis is Z anyway)
		float angleToTarget;
		Vector3 axis;
		rotation.ToAngleAxis(out angleToTarget, out axis);

		// if angle is negative, axis will be -Z and angleToTarget will be positive instead; change sign of angleToTarget if needed
		if (axis.z < 0) {
			angleToTarget *= -1;
		}

		// (c) with Eulerangles
//		float targetAngle = m_Target.eulerAngles.z;
//		float characterAngle = m_CharacterTransform.eulerAngles.z;
//		float angleToTarget = MathUtil.GetSmallestAngle(characterAngle, targetAngle);

		// end alternatives

		float targetRotationSpeed;  // target angular speed

		// if we are within radius, set target rotation to zero
		// note: unlike 'AI for Games', we prefer decelerating to zero velocity rather than doing nothing and waiting for drag to stop us
		if (angleToTarget < m_StopRadius) {
			targetRotationSpeed = 0f;
		} else {
			// compute final rotation speed, clamp if needed: before decelerate radius is reached, turn normally; else slow down
			targetRotationSpeed = Mathf.Min(angleToTarget / m_DecelerateRadius * m_MaxRotationSpeed, m_MaxRotationSpeed);
		}

		// set acceleration so that velocity is reached within timeToTargetVelocity (actually a bit longer, since accel will decrease when distance decreases)
		float angularAcceleration = (targetRotationSpeed - characterRigidbody2D.angularVelocity) / m_TimeToTargetVelocity;
		steering.angular = Mathf.Min(angularAcceleration, m_MaxAngularAcceleration);

		steering.linear = Vector2.zero;

		return steering;
	}
}

/// <summary>
/// Look where you are going
/// It is an Align movement with a fictive target ahead of your own movement, if any
/// If the character is pushed by another object, it will also look ahead
/// </summary>
public class LookAhead : Align {
	
	public LookAhead(Transform characterTr, float maxAngularAcceleration, float maxRotationSpeed, float stopRadius, float decelerateRadius, float timeToTargetVelocity) :
		base(characterTr, null, maxAngularAcceleration, maxRotationSpeed, stopRadius, decelerateRadius, timeToTargetVelocity) {
		var fictiveTargetGo = new GameObject("LookAhead_FictiveTarget");
		m_Target = fictiveTargetGo.transform;  // only its rotation matters
	}

	public override SteeringOutput GetSteering() {
		m_Target.rotation = MovementUtils.GetNewOrientation(m_CharacterTransform.rotation, characterRigidbody2D.velocity);
		return base.GetSteering();
	}

}

// TODO
public class Wander : SteeringMovement {

	// params
	float maxSpeed;
	float maxRotation;
	float rotationTimeInterval = 1f;  // the character changes orientation suddenly every rotationTimeInterval

	// state vars
	float timeBeforeNextRotation;

	public Wander(Transform characterTr, float _maxSpeed, float _maxRotation) : base(characterTr) {
		maxSpeed = _maxSpeed;
		maxRotation = _maxRotation;
		timeBeforeNextRotation = 0;  // later, when Wander is created only once, put that in Init/Reset movement method instead, called in state Enter()
	}

	public override SteeringOutput GetSteering() {
		// create the new structure for output
		var steering = new SteeringOutput();

		// the velocity is along the currently facing direction, at full speed
		steering.linear = m_CharacterTransform.up;
		steering.linear *= maxSpeed;

		// orientation varies with random binomial law around current orientation (forward for CCW angle in left-handed coord system), regularly
		timeBeforeNextRotation -= Time.deltaTime;  // deltaTime from FixedUpdate of FSMAIControl
		if (timeBeforeNextRotation < 0) {
			timeBeforeNextRotation = rotationTimeInterval;
			float randomAngle = (Random.value - Random.value) * maxRotation;  // between -maxRotation and +maxRotation, peak at 0, per second
			m_CharacterTransform.rotation = Quaternion.AngleAxis(randomAngle, Vector3.forward) * m_CharacterTransform.rotation;
		}

		return steering;
	}
}
